package model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import java.util.Map;

public class JsonAnyGetterModel {
    private String name;
    private Map<String, String> props;

    @JsonAnyGetter
    public Map<String, String> getProps() {
        return props;
    }
}
