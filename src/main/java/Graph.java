import java.util.ArrayList;
import java.util.Scanner;

public class Graph {
    static class Vertex {
        int id;
        ArrayList<Integer> adjancentVertices = new ArrayList<>();
        Vertex(int id) {
            this.id = id;
        }
    }

    // Attribute
    Vertex[] v;

    public static void main(String[] args) {
        Graph graph = new Graph();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of vertices: ");
        int n = scanner.nextInt();
        graph.v = new Vertex[n+1];

        for(int i=1;i<n+1;i++) {
            graph.v[i] = new Vertex(i);
        }

        System.out.println("Enter number of edges: ");
        int q = scanner.nextInt();

        for(int i=0;i<q;i++) {
            int src = scanner.nextInt();
            int dest = scanner.nextInt();
            graph.addEdge(src, dest);
        }

        for(int i=1;i<n+1;i++) {
            System.out.print(i + " -> ");
            for(int adjacentVertex: graph.v[i].adjancentVertices) {
                System.out.print(adjacentVertex + ", ");
            }
            System.out.println();
        }

        GraphAlgos.bfs(graph, 1);
    }

    // Behavior Or Function
    void addEdge(int source, int destination) {
        v[source].adjancentVertices.add(destination);
        v[destination].adjancentVertices.add(source);
    }

}
