import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class GraphAlgos {
    static void bfs(Graph g, int startVertex) {
        HashSet<Integer> visited = new HashSet<>();
        Queue<Integer> q = new LinkedList<>();
        q.add(startVertex);
        visited.add(startVertex);

        // For a simple line break in output
        System.out.println();

        while (!q.isEmpty()) {
            int id = q.poll();
            System.out.print(id + " -> ");
            for(int adjVtx: g.v[id].adjancentVertices) {
                if(!visited.contains(adjVtx)) {
                    q.add(adjVtx);
                    visited.add(adjVtx);
                }

            }
        }
    }
}
